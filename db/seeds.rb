#reset
# Rake::Task['db:reset'].invoke

#create admin user
puts 'creating admin'
admin = User.create!({:email => 'admin@ticketing-test.com', :password => 'ABC12abc'})
admin.update_attribute(:role, 3)


# create agents
puts 'creating agents'
agent = User.create!({:email => 'agent@ticketing-test.com', :password => 'ABC12abc'})
agent.update_attribute(:role, 2)
agent2 = User.create!({:email => 'agent2@ticketing-test.com', :password => 'ABC12abc'})
agent2.update_attribute(:role, 2)
agent3 = User.create!({:email => 'agent3@ticketing-test.com', :password => 'ABC12abc'})
agent3.update_attribute(:role, 2)

# create clients
puts 'creating clients'
User.create!({:email => 'elij@cbros.com', :password => 'ABC12abc'})
(1..4).to_a.each{|x| User.create!({:email => "#{x}_client@ticketing-test.com", :password => 'ABC12abc'}) }

# create tickets
texts = HTTParty.get("http://loripsum.net/api/15/short").split("\n")

puts 'creating ticket'
clients = User.all(:conditions => ['role = ?', 1])
clients.each do |client|
  puts "#{client.email} is creating tickets"
  texts.each{|text| str = ActionView::Base.full_sanitizer.sanitize(text); (Ticket.create({:user_id => client.id, :title => str.split(' ')[0..8].join(' '), :description => str}) if str.present?)}
end

tickets = Ticket.all
tickets.each do |ticket|
  rep = [agent, agent2, agent3].sample
  puts "#{rep.email} is processing the ticket #{ticket.id}"
  ticket.update_attributes({:agent_id => rep.id, :status => 2})
  author = [rep, ticket.user].sample
  puts "#{author.email} is responding to a ticket"
  texts[10..15].each{|text| str = ActionView::Base.full_sanitizer.sanitize(text); (TicketResponse.create({:message => str, :user_id => author.id, :ticket_id => ticket.id}) if str.present?) }
end