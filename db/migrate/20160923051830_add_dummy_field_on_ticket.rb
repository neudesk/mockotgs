class AddDummyFieldOnTicket < ActiveRecord::Migration
  def self.up
    add_column :tickets, :sample_attr, :string
  end

  def self.down
    remove_column :tickets, :sample_attr
  end
end
