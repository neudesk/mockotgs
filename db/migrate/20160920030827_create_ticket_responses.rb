class CreateTicketResponses < ActiveRecord::Migration
  def self.up
    create_table :ticket_responses do |t|
      t.integer :user_id, :null => false
      t.integer :ticket_id, :null => false
      t.text :message, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :ticket_responses
  end
end
