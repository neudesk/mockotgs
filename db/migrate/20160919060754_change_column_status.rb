class ChangeColumnStatus < ActiveRecord::Migration
  def self.up
    change_column :tickets, :status, :integer, :default => 1
  end

  def self.down
  end
end
