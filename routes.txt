      new_user_session GET    /users/sign_in                     {:controller=>"sessions", :action=>"new"}
          user_session POST   /users/sign_in                     {:controller=>"sessions", :action=>"create"}
  destroy_user_session GET    /users/sign_out                    {:controller=>"sessions", :action=>"destroy"}
     new_user_password GET    /users/password/new(.:format)      {:controller=>"passwords", :action=>"new"}
    edit_user_password GET    /users/password/edit(.:format)     {:controller=>"passwords", :action=>"edit"}
         user_password PUT    /users/password(.:format)          {:controller=>"passwords", :action=>"update"}
                       POST   /users/password(.:format)          {:controller=>"passwords", :action=>"create"}
 new_user_registration GET    /users/sign_up(.:format)           {:controller=>"registrations", :action=>"new"}
edit_user_registration GET    /users/edit(.:format)              {:controller=>"registrations", :action=>"edit"}
     user_registration PUT    /users(.:format)                   {:controller=>"registrations", :action=>"update"}
                       DELETE /users(.:format)                   {:controller=>"registrations", :action=>"destroy"}
                       POST   /users(.:format)                   {:controller=>"registrations", :action=>"create"}
       dashboard_index GET    /dashboard(.:format)               {:controller=>"dashboard", :action=>"index"}
                       POST   /dashboard(.:format)               {:controller=>"dashboard", :action=>"create"}
         new_dashboard GET    /dashboard/new(.:format)           {:controller=>"dashboard", :action=>"new"}
        edit_dashboard GET    /dashboard/:id/edit(.:format)      {:controller=>"dashboard", :action=>"edit"}
             dashboard GET    /dashboard/:id(.:format)           {:controller=>"dashboard", :action=>"show"}
                       PUT    /dashboard/:id(.:format)           {:controller=>"dashboard", :action=>"update"}
                       DELETE /dashboard/:id(.:format)           {:controller=>"dashboard", :action=>"destroy"}
                  root        /                                  {:controller=>"application", :action=>"index"}
                              /:controller/:action/:id           
                              /:controller/:action/:id(.:format) 
Loaded suite /home/dev/.rvm/gems/ruby-1.8.7-head@mockotgs/bin/rake
Started

Finished in 0.000158 seconds.

0 tests, 0 assertions, 0 failures, 0 errors
