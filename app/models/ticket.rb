class Ticket < ActiveRecord::Base

  attr_accessible :status, :title, :description, :user_id, :agent_id

  belongs_to :user
  has_many :ticket_responses

  validates_presence_of :title
  validates_presence_of :description

  def self.ticket_pool
    self.all(:conditions => ['status = ?', 1])
  end

  def self.agent_tickets(user)
    self.all(:conditions => ['agent_id = ?', user.id])
  end

  def self.customer_tickets(user)
    self.all(:conditions => ['user_id = ?', user.id])
  end

  def grab_ticket(user)
    self.update_attributes({:agent_id => user.id, :status => 2}) if user.agent?
  end

  def agent
    User.find(self.agent_id) if self.agent_id.present?
  end

  def owned(user)
    self.user_id == user.id
  end

end
