class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :http_authenticatable, :token_authenticatable, :confirmable, :lockable, :timeoutable and :activatable
  devise :registerable, :database_authenticatable, :recoverable,
         :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation

  has_many :tickets
  has_many :ticket_responses

  def customer?
    self.role == 1
  end

  def agent?
    self.role == 2
  end

  def admin?
    self.role == 3
  end

end
