class DashboardController < ApplicationController
  before_filter :authenticate_user!

  uses_tiny_mce(:options => AppConfig.default_mce_options, :only => [:index])

  def index
    method = current_user.admin? ? :all :
              current_user.agent? ? :agent_tickets :
                :customer_tickets
    @tickets = Ticket.try(method, current_user)
    @pool = Ticket.ticket_pool if current_user.agent?
  end

  def create
    @ticket = Ticket.new(params[:ticket])
    @sample_attr = @ticket.get_sample_attr
    if(@ticket.valid? and @ticket.save)
      flash[:notice] = 'Ticket has been submitted, we will get back to you as soon as possible.'
    else
      flash[:error] = @ticket.errors
    end
    respond_to do |format|
      format.js
    end
  end

  def show
    params[:page] ||= 1
    @ticket = Ticket.find(params[:id])
    @responses = @ticket.ticket_responses
    @pager = ::Paginator.new(@responses.count, 10) do |offset, per_page|
      @responses.find(:all, :limit => per_page, :offset => offset)
    end
    @pages = @pager.page(params[:page])
  end

  def create_response
    ticket = Ticket.find(params[:response][:ticket_id])
    if ticket.owned(current_user) || current_user.agent? || current_user.admin?
      response = TicketResponse.new(params[:response])
      if(response.valid? and response.save)
        response.ticket.grab_ticket(current_user) if current_user.agent?
        flash[:notice] = 'Success.'
      else
        flash[:error] = response.errors
      end
    end
    redirect_to dashboard_path(params[:response][:ticket_id])
  end

  def set_status
    ticket = Ticket.find(params[:ticket][:id])
    ticket.update_attribute(:status, params[:ticket][:status])
    redirect_to dashboard_path(params[:ticket][:id])
  end

end
