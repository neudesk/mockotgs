require 'spec_helper'

describe Ticket do
  before(:each) do
  end

  it 'should able to determine admin role' do
    admin = User.all(:conditions => ['email = ?', 'admin@ticketing-test.com']).first
    admin.admin?.should be true
  end

  it 'should able to determine customer role' do
    customer = User.all(:conditions => ['email = ?', 'elij@cbros.com']).first
    customer.customer?.should be true
  end

  it 'should able to determine agent role' do
    agent = User.all(:conditions => ['email = ?', 'agent@ticketing-test.com']).first
    agent.agent?.should be true
  end
end