require 'spec_helper'

describe Ticket do
  before(:each) do
    @customer = User.create!({:email => 'test@example.com', :password => 'ABC12abc'})
    @agent = User.create!({:email => 'agentest@example.com', :password => 'ABC12abc'})
    @agent.update_attribute(:role, 2)
    @valid_attributes = {
      :title => 'sample title',
      :description => 'sample description',
      :user_id => @customer,
      :agent_id => nil
    }
    @ticket = Ticket.create!(@valid_attributes)
    # load Rails.root + "db/seeds.rb"
  end

  it 'should able to grab ticket' do
    @ticket.grab_ticket(@agent)
    @ticket.agent_id.should == @agent.id
  end

  it 'should able to get agent from ticket' do
    @ticket.grab_ticket(@agent)
    @ticket.agent.email.should == @agent.email
  end

  it 'should be able to get all agents ticket' do
    agent = User.all(:conditions => ['email = ?', 'agent@ticketing-test.com']).first
    tickets = Ticket.all(:conditions => ['agent_id = ?', agent.id]).count
    agent_tickets = Ticket.agent_tickets(agent).count
    agent_tickets.should == tickets
  end

  it 'should be able to get all customers ticket' do
    customer = User.all(:conditions => ['email = ?', '2_client@ticketing-test.com'])
    tickets = Ticket.all(:conditions => ['user_id = ?', customer.id]).count
    customer_tickets = Ticket.customer_tickets(customer).count
    customer_tickets.should == tickets
  end

  it 'sample attr should not save' do
    ticket = Ticket.last
    msg = 'this message should not be saved!'
    ticket.update_attributes({:sample_attr => msg})
    ticket.sample_attr.should_not be msg
  end

end
