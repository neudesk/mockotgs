# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_mockotgs_session',
  :secret      => '004ac454e637e7e80b78e43ed3e95f51d33bb46f950a4d10f7c5c1da9c22b7779795013d5949429ff8fcec0dc37477a58ad8a42b08b4a477fc8eb4111f0a46a4'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
